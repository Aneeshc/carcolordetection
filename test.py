import threading
import pymysql
import pandas as pd
from skimage import io
import os
import pandas as pd
from pyimagesearch.colorlabeler import ColorLabeler
from sklearn.cluster import KMeans
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
import psycopg2
import os
import time
import imgRocc









def print_cube(name,img):
    from imageai.Detection import ObjectDetection
    #testimg = img.copy()
    execution_path = os.getcwd()
    detector = ObjectDetection()
    detector.setModelTypeAsRetinaNet()
    detector.setModelPath(os.path.join(execution_path, "resnet50_coco_best_v2.0.1.h5"))
    detector.loadModel()
    detections, extracted_images = detector.detectObjectsFromImage(
        input_image= img,
        input_type='array',
        output_image_path= os.path.join(execution_path, "imagenew.jpg"),#'/home/ai/working_env/imagecolor/temp/%s/'%(name),
        extract_detected_objects=True)

    high_pp = 0
    for jsons in detections:
        print(name,jsons['name'])
        if jsons['name'] in ['car', 'truck']:
            if high_pp < jsons['percentage_probability']:
                high_pp = jsons['percentage_probability']
                array = jsons['box_points']
                # print('prob', jsons['percentage_probability'])

            # input()
    x1, y1, x2, y2 = array[0], array[1], array[2], array[3]
    return 'g'
    """
    function to print cube of given num
    """


def url_to_image(url):
    try:
        image = io.imread(url)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        return True,image
    except:
        return  False,None





def getColor(url,name):
    flag,img = url_to_image(url)
    print(img.shape)
    print_cube(name,img)



#getColor(,'test')

t1 = threading.Thread(target=imgRocc.getColor, args=('https://s3.us-west-2.amazonaws.com/gogocar-dev/media/vehicles/detailsphoto/lg1/2T1BURHE9GC657848_1.jpg','test1',))
t2 = threading.Thread(target=imgRocc.getColor, args=('https://s3.us-west-2.amazonaws.com/gogocar-dev/media/vehicles/detailsphoto/lg1/2T1BURHE9GC657848_1.jpg','test2',))
t1.start()
t2.start()
# import pymysql
# import pandas as pd
#
# def get_database_connection():
#     database = pymysql.connect(
#         host="kapowdb.gogocar.com",
#         user="kapow_root",
#         passwd="K@nn@Pp@NUnN!!",
#         db="AI_data_hub")
#     return database
#
# def updateDB(flag, row,RGB, Color):
#     connection = get_database_connection()
#     cursor = connection.cursor()
#     query = "UPDATE AI_data_hub.color_recognition SET GenericColor= '%s' , Hexcode = '%s' , RecognitionStatus= '%s' WHERE Vin='%s' "%(Color, RGB, flag, row.Vin)
#     print (query)
#     cursor.execute(query)
#     cursor.close()
#     connection.commit()
#     connection.close()
#
#
#
# conn = get_database_connection()
# table = pd.read_sql("select * from AI_data_hub.color_recognition where Vin = '2G1155SL0F9290127' ", conn)
# print (str(True))
# for index, row in table.iterrows():
#     print(row)
#     print( row)
    #updateDB(str(True) , row, '-'.join(map (str, [255, 255, 255])), 'RED')



# import os
# import pandas as pd
# import cv2
# import numpy as np
#
#
# def showImage(img):
#     cv2.imshow('image',img)
#     cv2.waitKey(0)
#     return
#
#
# def loadImage():
#     execution_path = os.getcwd()
#     img = cv2.imread(os.path.join(execution_path, "image111.jpg"))
#     return img
#
# def centerImage(imgcp):
#     img = imgcp.copy()
#     y1,x1,y2,x2= getImgShape(img.copy())
#     cx, cy = int((x1 + x2) / 2), int((y1 + y2) / 2)
#     inx, iny = int((x1 + x2) / 4), int((y1 + y2) / 4)
#     print(cx, cy)
#     print(inx, iny)
#     img = img[cy - iny: cy + iny, x1: x2]  # cx - xlen : cx + xlen ]
#     return img
# def getImgShape(img):
#
#      return 0, 0 , img.shape[0] , img.shape[1]
#
# def splitImage(img,grid_x = 4,grid_y=1):
#     y1, x1, y2, x2 = getImgShape(img.copy())
#     avgx = int( (x2 - x1) / grid_x )
#     avgy = int( (y2 - y1) / grid_y )
#     ximgary = [x * avgx for x in range(0, grid_x + 1)]
#     yimgary = [y * avgy for y in range(0, grid_y + 1)]
#     return [ximgary,yimgary]
#
# def drawGrid(imgCord,img):
#     noxgrid = len( imgCord[0] ) - 1
#     noygrid = len( imgCord[1] ) - 1
#     imgArray = np.empty((noygrid,noxgrid), dtype=object)
#     print(imgArray)
#     for j in range(0,noygrid):
#         for i in range(0, noxgrid):
#             #cv2.rectangle(img, (imgCord[0][i], imgCord[1][j]), ( imgCord[0][i+1], imgCord[1][j+1] ), (255, 0, 0), 2)
#             #showImage(img)
#             imgArray[j][i] = img[imgCord[1][j]:imgCord[1][j+1] , imgCord[0][i]:imgCord[0][i+1] ]
#             #showImage( imgArray[j][i] )
#             #showImage( imgArray[i] )#img[imgCord[1][j]:imgCord[1][j+1] , imgCord[0][i]:imgCord[0][i+1] ] )
#     return imgArray
#
# def getLessNoiseImages(imgs):
#     minstd = (np.inf,None,None)
#     noxgrid = imgs.shape[1]
#     noygrid = imgs.shape[0]
#     print(imgs.shape)
#     for y in range(0,noygrid):
#         for x in range(0,noxgrid):
#
#             image = imgs[y][x].reshape((imgs[y][x].shape[0] * imgs[y][x].shape[1], 3))
#             df = pd.DataFrame(data=image, columns=['B', 'G', 'R'])
#             d = df["B"].std() + df["G"].std() + df["R"].std()
#             print(d)
#             #showImage(imgs[y][x])
#             if(minstd[0] >d):
#                 minstd = (d,y,x)
#     return minstd
#
# img = loadImage()
# centerimg = centerImage(img)
# showImage(centerimg)
#
# subImageCordArry = splitImage(centerimg.copy(),6)
# gridimgs = drawGrid(subImageCordArry,centerimg)
# dis,y,x = getLessNoiseImages(gridimgs)
# showImage(gridimgs[y][x])
#


