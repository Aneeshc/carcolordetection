From python:3.6.4
RUN mkdir /home/app
WORKDIR /home/app
ADD . /home/app
RUN cd /home/app
RUN pip install --upgrade pip && pip install -r requirement.txt
CMD python colordetection.py
