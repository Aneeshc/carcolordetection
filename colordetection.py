import pymysql
import pandas as pd
from skimage import io
from imageai.Detection import ObjectDetection
import pandas as pd
from pyimagesearch.colorlabeler import ColorLabeler
from sklearn.cluster import KMeans
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
import os

execution_path = os.getcwd()
detector = ObjectDetection()
detector.setModelTypeAsRetinaNet()
detector.setModelPath(os.path.join(execution_path, "resnet50_coco_best_v2.0.1.h5"))
detector.loadModel()



def preprocess(rgb):
    blurred = cv2.GaussianBlur(rgb, (5, 5), 0)
    return blurred


def find_countours(binary_img):
    cnts = cv2.findContours(binary_img.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    return cnts

def get_dominant_color(image, k=3, image_processing_size=None):
    image = image.reshape((image.shape[0] * image.shape[1], 3))
    df = pd.DataFrame(data=image,columns=['B', 'G', 'R'])
    clt = KMeans(n_clusters=k)
    labels = clt.fit_predict(image)
    df["label"] = labels
    label_counts = Counter(labels)
    dominant_color = clt.cluster_centers_[label_counts.most_common(1)[0][0]]
    unique_l, counts_l = np.unique(clt.labels_, return_counts=True)
    sort_ix = np.argsort(counts_l)
    sort_ix = sort_ix[::-1]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    x_from = 0.05
    for cluster_center in clt.cluster_centers_[sort_ix[0:1]]:
        ax.add_patch(patches.Rectangle((x_from, 0.05), 0.29, 0.9, alpha=None,
                                       facecolor='#%02x%02x%02x' % (
                                       int(cluster_center[2]), int(cluster_center[1]), int(cluster_center[0]))))
        x_from = x_from + 0.31

    #plt.show(block = False)
    #plt.pause(3)
    #plt.close()
    finalImageArray = [int(cluster_center[2]), int(cluster_center[1]), int(cluster_center[0])]
    return finalImageArray

def imageRecong(img,flag, row):
    testimg = img.copy()
    detections, extracted_images = detector.detectObjectsFromImage(
        input_image=img.copy(),  # os.path.join(execution_path , "image111.jpg"),
        output_image_path=os.path.join(execution_path, "imagenew.jpg"),
        input_type='array',
        extract_detected_objects=True)


    high_pp,array = 0,[]
    for jsons in detections:
       # print(jsons['name'])
        if jsons['name'] in[ 'car','truck' ]:

            area = (jsons['box_points'][2] - jsons['box_points'][0]) + (
                    jsons['box_points'][3] - jsons['box_points'][1])
            if high_pp < area:
                high_pp = area
                array = jsons['box_points']
                #print('prob', jsons['percentage_probability'])

            # input()
    if( len(array) == 0):
       return updateDB('False', row, 'car not avialable', '')



    x1, y1, x2, y2 = array[0], array[1], array[2], array[3]
    img = img[y1:y2, x1:x2]
    #showImage(img)
    centerimg = centerImage(img)
    #showImage(centerimg)
    subImageCordArry = splitImage(centerimg.copy(), 4)
    gridimgs = drawGrid(subImageCordArry, centerimg)
    dis, y, x = getLessNoiseImages(gridimgs)
    #showImage(gridimgs[y][x])


    subImageCordArry = splitImage(gridimgs[y][x], 2,2)
    gridimgs = drawGrid(subImageCordArry, gridimgs[y][x])
    dis, y, x = getLessNoiseImages(gridimgs)
    #showImage(gridimgs[y][x])

    #img = preprocess(img)
    #cv2.imshow('image',img)
    #cv2.waitKey(0)
    RGB = get_dominant_color(gridimgs[y][x])

    cl = ColorLabeler()
    COLOR = cl.newlabel(RGB)
    updateDB(str(flag), row, '-'.join(map(str, RGB)), COLOR)
    #return RGB,



def get_database_connection():
    database = pymysql.connect(
        host="kapowdb.gogocar.com",
        user="kapow_root",
        passwd="K@nn@Pp@NUnN!!",
        db="AI_data_hub")
    return database


def url_to_image(url):
    try:
        image = io.imread(url)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        return True,image
    except:
        return  False,None





def get_data_fromdb(df,query,limit = 1000,offset = 0):

    conn = get_database_connection()
    dbquery = query + " limit %s offset %s "%( str(limit),str(offset) )
    print(dbquery)
    dbresult = pd.read_sql(dbquery,conn)
    conn.close()
    if(len(dbresult) == 0):
        return df
    df = df.append([dbresult])
    offset = limit+offset
    return get_data_fromdb(df,query,limit,offset)


def showImage(img,sleep = 3000):
    cv2.imshow('image',img)
    cv2.waitKey(sleep)
    cv2.destroyAllWindows()
    return


def loadImage():
    execution_path = os.getcwd()
    img = cv2.imread(os.path.join(execution_path, "image111.jpg"))
    return img

def centerImage(imgcp):
    img = imgcp.copy()
    y1,x1,y2,x2= getImgShape(img.copy())
    cx, cy = int((x1 + x2) / 2), int((y1 + y2) / 2)
    inx, iny = int((x1 + x2) / 6), int((y1 + y2) / 6)
    #print(cx, cy)
    #print(inx, iny)
    img = img[cy - iny: cy + iny, x1: x2]  # cx - xlen : cx + xlen ]
    return img
def getImgShape(img):

     return 0, 0 , img.shape[0] , img.shape[1]

def splitImage(img,grid_x = 4,grid_y=1):
    y1, x1, y2, x2 = getImgShape(img.copy())
    avgx = int( (x2 - x1) / grid_x )
    avgy = int( (y2 - y1) / grid_y )
    ximgary = [x * avgx for x in range(0, grid_x + 1)]
    yimgary = [y * avgy for y in range(0, grid_y + 1)]
    return [ximgary,yimgary]

def drawGrid(imgCord,img):
    noxgrid = len( imgCord[0] ) - 1
    noygrid = len( imgCord[1] ) - 1
    imgArray = np.empty((noygrid,noxgrid), dtype=object)

    for j in range(0,noygrid):
        for i in range(0, noxgrid):
            #cv2.rectangle(img, (imgCord[0][i], imgCord[1][j]), ( imgCord[0][i+1], imgCord[1][j+1] ), (255, 0, 0), 2)
            #showImage(img)
            imgArray[j][i] = img[imgCord[1][j]:imgCord[1][j+1] , imgCord[0][i]:imgCord[0][i+1] ]
            #showImage( imgArray[j][i] )
            #showImage( imgArray[i] )#img[imgCord[1][j]:imgCord[1][j+1] , imgCord[0][i]:imgCord[0][i+1] ] )
    return imgArray

def getLessNoiseImages(imgs):
    minstd = (np.inf,None,None)
    noxgrid = imgs.shape[1]
    noygrid = imgs.shape[0]
    #print(imgs.shape)
    for y in range(0,noygrid):
        for x in range(0,noxgrid):

            image = imgs[y][x].reshape((imgs[y][x].shape[0] * imgs[y][x].shape[1], 3))
            df = pd.DataFrame(data=image, columns=['B', 'G', 'R'])
            d = df["B"].std() + df["G"].std() + df["R"].std()
            #print(d)
            showImage(imgs[y][x])
            if(minstd[0] >d):
                minstd = (d,y,x)
    return minstd

def updateDB(flag, row,RGB, Color):
    print(flag, RGB, Color)
    connection = get_database_connection()
    cursor = connection.cursor()
    query = "UPDATE AI_data_hub.color_recognition SET GenericColor= '%s' , Hexcode = '%s' , RecognitionStatus= '%s' WHERE Vin='%s' "%(Color, RGB, flag, row.Vin)
    cursor.execute(query)
    cursor.close()
    connection.commit()
    connection.close()

table = get_data_fromdb( pd.DataFrame(),"SELECT * FROM AI_data_hub.color_recognition where RecognitionStatus is null",1000,0 )
#conn = get_database_connection()
#table = pd.read_sql("select * from AI_data_hub.color_recognition   limit 1 offset 75", conn)
#conn.close()
print('new images length',len(table) )

for index,row in table.iterrows():
    imageURL = row
    print(index,' ', row.ImageUrl)
    flag,image = url_to_image(row.ImageUrl)
    if not flag:
        updateDB(str(flag), row, 'image not avialable', '')
        continue
    #image = loadImage()
    img = image.copy()
    showImage(img)
    imageRecong(img, str(flag), row)
    #break






