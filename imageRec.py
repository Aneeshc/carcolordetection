from imageai.Detection import ObjectDetection
import os
import pandas as pd
from pyimagesearch.colorlabeler import ColorLabeler
from sklearn.cluster import KMeans
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
import psycopg2
















execution_path = os.getcwd()
detector = ObjectDetection()
detector.setModelTypeAsRetinaNet()
detector.setModelPath(os.path.join(execution_path, "resnet50_coco_best_v2.0.1.h5"))
detector.loadModel()
# detections = detector.detectObjectsFromImage(input_image=os.path.join(execution_path , "image.jpg"),
img = cv2.imread(os.path.join(execution_path, "image111.jpg"))

testimg = img.copy()
# print ('shape', img)imageReg
detections, extracted_images = detector.detectObjectsFromImage(
    input_image=img.copy(),  # os.path.join(execution_path , "image111.jpg"),
    output_image_path=os.path.join(execution_path, "imagenew.jpg"),
    input_type='array',
    extract_detected_objects=True)
print(extracted_images)

high_pp = 0
for jsons in detections:
    print(jsons['name'])
    if jsons['name'] in[ 'car','truck' ]:
        if high_pp < jsons['percentage_probability']:
            high_pp = jsons['percentage_probability']
            array = jsons['box_points']
            print('prob', jsons['percentage_probability'])

        # input()
x1, y1, x2, y2 = array[0], array[1], array[2], array[3]
cx,cy = int((x1+x2)/2) , int((y1+y2)/2)
inx,iny = int((x2-x1)/4) , int((y2-y1)/4)
img = img[y1+iny:y2-iny, x1+inx:x2-inx]
img = preprocess(img)
cv2.imshow('image',img)
cv2.waitKey(0)
list1 = get_dominant_color(img)
cl = ColorLabeler()
print(list1,cl.newlabel(list1))


def preprocess(rgb):
    blurred = cv2.GaussianBlur(rgb, (5, 5), 0)
    return blurred
    gray = cv2.cvtColor(blurred.copy(), cv2.COLOR_BGR2GRAY)
    lab = cv2.cvtColor(blurred.copy(), cv2.COLOR_BGR2LAB)
    #   print ('gray_shape', gray.shape)
    binary = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY)[1]
    thresh = binary.max() - binary
    return thresh, lab


def find_countours(binary_img):
    cnts = cv2.findContours(binary_img.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    # cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    return cnts





def get_dominant_color(image, k=3, image_processing_size=None):


    image = image.reshape((image.shape[0] * image.shape[1], 3))
    df = pd.DataFrame(data=image,columns=['B', 'G', 'R'])
    clt = KMeans(n_clusters=k)
    labels = clt.fit_predict(image)
    df["label"] = labels
    print(df["B"].std(),df["G"].std(),df["R"].std())
    label_counts = Counter(labels)
    dominant_color = clt.cluster_centers_[label_counts.most_common(1)[0][0]]
    unique_l, counts_l = np.unique(clt.labels_, return_counts=True)
    sort_ix = np.argsort(counts_l)
    sort_ix = sort_ix[::-1]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    x_from = 0.05

    for cluster_center in clt.cluster_centers_[sort_ix[0:2]]:
        ax.add_patch(patches.Rectangle((x_from, 0.05), 0.29, 0.9, alpha=None,
                                       facecolor='#%02x%02x%02x' % (
                                       int(cluster_center[2]), int(cluster_center[1]), int(cluster_center[0]))))
        print(int(cluster_center[2]), int(cluster_center[1]), int(cluster_center[0]))
        x_from = x_from + 0.31

    plt.show()
    finalImageArray = [int(cluster_center[2]), int(cluster_center[1]), int(cluster_center[0])]
    return finalImageArray


# execution_path = os.getcwd()
# detector = ObjectDetection()
# detector.setModelTypeAsRetinaNet()
# detector.setModelPath(os.path.join(execution_path, "resnet50_coco_best_v2.0.1.h5"))
# detector.loadModel()
# detections = detector.detectObjectsFromImage(input_image=os.path.join(execution_path , "image.jpg"),



