# import the necessary packages
from scipy.spatial import distance as dist
from collections import OrderedDict
import numpy as np
import cv2
import pandas as pd









 
class ColorLabeler:
	def __init__(self):
		# initialize the colors dictionary, containing the color
		# name as the key and the RGB tuple as the value
		color =  pd.read_csv('testcolorsample.csv')
		self.colorFrame = color.copy()
		color['color_value'] = color[['R', 'G','B']].apply(tuple, axis=1)
		color_label = {}
		for index, row in color.iterrows():
			color_label[ row["Code"] ] = row["color_value"]
		colors = OrderedDict(color_label)

		self.lab = np.zeros((len(colors), 1, 3), dtype="uint8")
		self.colorNames = []
 
		# loop over the colors dictionary
		for (i, (name, rgb)) in enumerate(colors.items()):
			# update the L*a*b* array and the color names list
			self.lab[i] = rgb
			self.colorNames.append(name)
 
		#convert the L*a*b* array from the RGB color space
		# to L*a*b*
		#self.lab = cv2.cvtColor(self.lab, cv2.COLOR_RGB2LAB)



	def newlabel(self, rgb):
		# construct a mask for the contour, then compute the
		# average L*a*b* value for the masked region
		self.newlab = np.zeros((1, 1, 3), dtype="uint8")
		self.newlab[0] = rgb
		#self.newlab = cv2.cvtColor(self.lab, cv2.COLOR_RGB2LAB)
		minDist = (np.inf, None)
		# loop over the known L*a*b* color values
		for (i, row) in enumerate(self.lab):
			d = dist.cdist(row, self.newlab[0])
			if d[0][0] < minDist[0]:
				minDist = (d, i,)
		#print( )
		return self.colorFrame.loc[ self.colorFrame["Code"] == self.colorNames[minDist[1]] ]["Color"].values[0]